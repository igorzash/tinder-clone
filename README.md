# tinder-clone

Project that aims to provide generic dating app features, like:
- location, age, sex (sex preference) based matching
- like/dislike reactions
- mutual likes detection

# About

What's used:

 - Django (DRF)
 - GeoDjango (PostGIS)
 - djangorestframework-simplejwt
 - python-dateutil
 - drf-yasg

# API specification

Swagger UI is available at `/swagger/`.

- GET `/users/?longitude=x.x&latitude=y.y` list users (if location isn't provided last one is used)

- POST `/users/` register

- PUT `/users/{id}` update user data

---

- POST `/reactions/like/{id}` like someone

- POST `/reactions/dislike/{id}` dislike someone

- GET `/reactions/mutual` list mutual likes

---

- POST `/token` get jwt token

- POST `/token/refresh` refresh jwt token

# Authorization

Get jwt access token and put in `Authorization` HTTP header.

```http
Authorization: Bearer {your jwt goes here}
```

# Usage

```sh
docker-compose up -d

docker-compose exec web ./manage.py test

docker-compose exec web ./manage.py fillrandom {count} {min_long} {max_long} {min_lat} {max_lat} # fills database with random users (100 by default, none of parameters are required)

docker-compose exec web ./manage.py react like user1 user2

docker-compose exec web ./manage.py react dislike user1 user2

docker-compose exec web ./manage.py gettoken user1 # get jwt access token
```



