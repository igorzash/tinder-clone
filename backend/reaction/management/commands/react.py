"""React command."""

from django.core.management.base import BaseCommand

from user.models import CustomUser


class Command(BaseCommand):  # pylint:disable=missing-class-docstring
    help = "Lets you manually like/dislike someone as any user."

    def add_arguments(self, parser):
        parser.add_argument("action", type=str)
        parser.add_argument("username1", type=str)
        parser.add_argument("username2", type=str)

    def handle(self, *args, **options):
        assert options["action"] in ["like", "dislike"]

        if options["action"] == "like":
            CustomUser.objects.get(username=options["username1"]).likes.add(
                CustomUser.objects.get(username=options["username2"])
            )
        else:
            CustomUser.objects.get(username=options["username1"]).dislikes.add(
                CustomUser.objects.get(username=options["username2"])
            )
