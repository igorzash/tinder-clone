"""Reaction app URL patterns."""

from django.urls import path

from .views import ReactionView, MutualLikesView


urlpatterns = [
    path("reaction/<str:action>/<int:pk>/", ReactionView.as_view()),
    path("reaction/mutual/", MutualLikesView.as_view()),
]
