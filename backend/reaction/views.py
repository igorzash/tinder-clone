"""Reaction app views."""

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import permissions
from rest_framework.exceptions import ValidationError
from rest_framework.pagination import PageNumberPagination

from user.models import CustomUser
from user.serializers import UserSerializer


class ReactionView(APIView):
    """Reaction view. Like/Dislike functionality."""

    permission_classes = [permissions.IsAuthenticated]

    def post(self, request, **kwargs):  # pylint:disable=no-self-use
        """Handles the reaction event."""

        if kwargs["action"] not in ["like", "dislike"]:
            raise ValidationError()

        target_user = CustomUser.objects.get(pk=kwargs["pk"])

        if kwargs["action"] == "like":
            request.user.likes.add(target_user)
        else:
            request.user.dislikes.add(target_user)

        return Response(status=200)


class MutualLikesView(APIView, PageNumberPagination):
    """Mutual likes view."""

    permission_classes = [permissions.IsAuthenticated]

    def get(self, request):
        """Retrieves user who liked back."""

        results = request.user.likes.filter(likes=request.user).order_by("-date_joined")

        page = self.paginate_queryset(results, request)
        serializer = UserSerializer(page, many=True)
        return self.get_paginated_response(serializer.data)
