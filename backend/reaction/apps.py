"""Reaction config."""
from django.apps import AppConfig


class ReactionConfig(AppConfig):
    """Reaction app config."""

    default_auto_field = "django.db.models.BigAutoField"
    name = "reaction"
