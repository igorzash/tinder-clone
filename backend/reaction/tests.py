"""Reaction app tests."""

from utils.auth_testcase import AuthTestCase
from user.models import CustomUser


class ReactionTestCase(AuthTestCase):
    """Test UserViewSet."""

    def test_like(self):
        """Test like functionality."""

        user2 = CustomUser.objects.create(email="user2@mail.com", username="user2")

        self.client.post(f"/reaction/like/{user2.id}/")

        self.assertEqual(self.user.likes.count(), 1)

        user2.delete()
        self.user.likes.set([])

    def test_dislike(self):
        """Test dislike functionality."""

        user2 = CustomUser.objects.create(email="user2@mail.com", username="user2")

        self.client.post(f"/reaction/dislike/{user2.id}/")

        self.assertEqual(self.user.dislikes.count(), 1)

        user2.delete()
        self.user.dislikes.set([])

    def test_mutuallike(self):
        """Test mutual likes."""

        user2 = CustomUser.objects.create(email="user2@mail.com", username="user2")
        user2.likes.set([self.user])
        user2.save()

        user3 = CustomUser.objects.create(email="user3@mail.com", username="user3")

        self.user.likes.set([user2])
        self.user.save()

        self.assertEqual(self.client.get("/reaction/mutual/").json()["count"], 1)

        user2.delete()
        user3.delete()
        self.user.likes.set([])
