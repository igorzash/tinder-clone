"""Auth testcase."""

from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import RefreshToken

from user.models import CustomUser


class AuthTestCase(TestCase):
    """TestCase with user auth."""

    client: APIClient
    user: CustomUser

    def tearDown(self):
        CustomUser.objects.all().delete()

    def setUp(self):
        """Sets up environment for tests."""
        CustomUser.objects.all().delete()
        self.user = CustomUser.objects.create(email="user1@mail.com", username="user1")

        self.client = APIClient()
        self.client.credentials(
            HTTP_AUTHORIZATION="Bearer "
            + str(RefreshToken.for_user(self.user).access_token)
        )
