"""User app tests."""

from datetime import datetime

from django.contrib.gis.geos import Point
from dateutil.relativedelta import relativedelta

from utils.auth_testcase import AuthTestCase
from .models import CustomUser


class UserViewSetTestCase(AuthTestCase):
    """Test UserViewSet."""

    def test_creates_user(self):
        """Test registration."""
        self.client.post(
            "/users/",
            {"username": "user2", "email": "user2@mail.com", "password": "pass2"},
        )

        self.assertEqual(CustomUser.objects.all().count(), 2)

        CustomUser.objects.get(username="user2").delete()

    def test_excludes_requester(self):
        """Make sure users don't see themselves on the list."""
        data = self.client.get("/users/").json()

        self.assertEqual(data["count"], 0)

    def test_sorts_by_location(self):
        """Make sure users get sorted by location."""
        locations = [(80.5, 80), (50.1, 50.001), (50.1, 50.1)]

        users = []

        for i, location in enumerate(locations):
            users.append(
                CustomUser.objects.create(
                    email=f"user{i+2}@mail.com",
                    username=f"user{i+2}",
                    location=Point(*location),
                )
            )

        results = self.client.get("/users/?longitude=50&latitude=50").json()["results"]
        self.assertListEqual(
            list(map(lambda x: x["username"], results)), ["user3", "user4", "user2"]
        )

        # if coordinates aren't provided should use previous ones
        results = self.client.get("/users/").json()["results"]
        self.assertListEqual(
            list(map(lambda x: x["username"], results)), ["user3", "user4", "user2"]
        )

        results = self.client.get("/users/?longitude=70&latitude=70").json()["results"]
        self.assertListEqual(
            list(map(lambda x: x["username"], results)), ["user2", "user4", "user3"]
        )

        for user in users:
            user.delete()

    def test_filters_age_requester_preferences(self):
        """Make sure users get filtered by age preferences provided by the requester user."""
        ages = [18, 25, 30, 45, 53]
        birth_dates = map(
            lambda x: datetime.now() - relativedelta(years=x, days=1), ages
        )

        users = []

        for i, birth_date in enumerate(birth_dates):
            users.append(
                CustomUser.objects.create(
                    email=f"user{i+2}@mail.com",
                    username=f"user{i+2}",
                    birth_date=birth_date,
                )
            )

        def test_results_count(min_age, max_age, count):
            self.user.age_preference_min = min_age
            self.user.age_preference_max = max_age
            self.user.save()

            self.assertEqual(self.client.get("/users/").json()["count"], count)

        test_results_count(-1, -1, 5)
        test_results_count(-1, 20, 1)
        test_results_count(18, 18, 1)
        test_results_count(20, 20, 0)
        test_results_count(25, 45, 3)

        for user in users:
            user.delete()

    def test_filters_age_preferences_of_others(self):
        """Make sure users get filtered by their age preferences related to the requester."""

        preferences = [(18, 30), (40, 50), (30, 35), (20, 20)]

        users = []

        for i, preference in enumerate(preferences):
            users.append(
                CustomUser.objects.create(
                    email=f"user{i+2}@mail.com",
                    username=f"user{i+2}",
                    age_preference_min=preference[0],
                    age_preference_max=preference[1],
                )
            )

        def test_results_count(age, count):
            self.user.birth_date = datetime.now() - relativedelta(years=age, days=1)
            self.user.save()

            self.assertEqual(self.client.get("/users/").json()["count"], count)

        test_results_count(18, 1)
        test_results_count(20, 2)
        test_results_count(40, 1)
        test_results_count(50, 1)
        test_results_count(30, 2)

        for user in users:
            user.delete()

    def test_filters_sex_requester_preference(self):
        """Make sure users get filtered by sex preferences provided by the requester user."""
        sexes = [
            CustomUser.Sex.MALE,
            CustomUser.Sex.FEMALE,
            CustomUser.Sex.NOT_SPECIFIED,
        ]

        users = []

        for i, sex in enumerate(sexes):
            users.append(
                CustomUser.objects.create(
                    email=f"user{i+2}@mail.com", username=f"user{i+2}", sex=sex
                )
            )

        def test_results_count(sex_preference, count):
            self.user.sex_preference = sex_preference
            self.user.save()

            self.assertEqual(self.client.get("/users/").json()["count"], count)

        test_results_count(CustomUser.SexPreference.MALE, 1)
        test_results_count(CustomUser.SexPreference.FEMALE, 1)
        test_results_count(CustomUser.SexPreference.MALE_AND_FEMALE, 3)
        test_results_count(CustomUser.SexPreference.NOT_SPECIFIED, 3)

        for user in users:
            user.delete()

    def test_filter_sex_preferences_of_others(self):
        """Make sure users get filtered by their sex preferences related to the requester."""
        sex_preferences = [
            CustomUser.SexPreference.MALE,
            CustomUser.SexPreference.FEMALE,
            CustomUser.SexPreference.MALE_AND_FEMALE,
            CustomUser.SexPreference.NOT_SPECIFIED,
        ]

        users = []

        for i, sex_preference in enumerate(sex_preferences):
            users.append(
                CustomUser.objects.create(
                    email=f"user{i+2}@mail.com",
                    username=f"user{i+2}",
                    sex_preference=sex_preference,
                )
            )

        def test_results_count(sex, count):
            self.user.sex = sex
            self.user.save()

            self.assertEqual(self.client.get("/users/").json()["count"], count)

        test_results_count(CustomUser.Sex.MALE, 3)
        test_results_count(CustomUser.Sex.FEMALE, 3)
        test_results_count(CustomUser.Sex.NOT_SPECIFIED, 2)

        for user in users:
            user.delete()
