"""Fills database with random data."""

from random import uniform, randint
from django.contrib.gis.geos import Point
from django.core.management.base import BaseCommand

from user.models import CustomUser


class Command(BaseCommand):  # pylint:disable=missing-class-docstring
    help = "Fill database with random people."

    def add_arguments(self, parser):
        parser.add_argument("count", type=int, nargs="?", default=100)
        parser.add_argument("min_long", type=float, nargs="?", default=45)
        parser.add_argument("max_long", type=float, nargs="?", default=52)
        parser.add_argument("min_lat", type=float, nargs="?", default=25)
        parser.add_argument("max_lat", type=float, nargs="?", default=40)

    def handle(self, *args, **options):
        for i in range(options["count"]):
            CustomUser.objects.create(
                username=f"user{i}",
                email=f"user{i}@mail.com",
                location=Point(
                    uniform(options["min_long"], options["max_long"]),
                    uniform(options["min_lat"], options["max_lat"]),
                ),
                sex=randint(0, 2),
                sex_preference=randint(0, 3),
            )
