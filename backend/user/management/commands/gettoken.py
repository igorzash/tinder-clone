"""Get user jwt access token."""

from django.core.management.base import BaseCommand
from rest_framework_simplejwt.tokens import RefreshToken

from user.models import CustomUser


class Command(BaseCommand):  # pylint:disable=missing-class-docstring
    help = "Lets you see mutual likes of any user."

    def add_arguments(self, parser):
        parser.add_argument("username", type=str)

    def handle(self, *args, **options):
        user = CustomUser.objects.get(username=options["username"])

        print(str(RefreshToken.for_user(user).access_token))
