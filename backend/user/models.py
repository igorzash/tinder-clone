"""User model."""

from django.contrib.gis.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.utils import timezone

from .managers import CustomUserManager


class CustomUser(AbstractBaseUser):
    """Custom user model."""

    class Sex(models.IntegerChoices):
        """Sex choices."""

        NOT_SPECIFIED = 0
        MALE = 1
        FEMALE = 2

    class SexPreference(models.IntegerChoices):
        """Sex preference choices."""

        NOT_SPECIFIED = 0
        MALE = 1
        FEMALE = 2
        MALE_AND_FEMALE = 3

    username = models.CharField(max_length=20)
    email = models.EmailField(unique=True)

    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    is_superuser = models.BooleanField(default=False)

    date_joined = models.DateTimeField(default=timezone.now)

    sex = models.IntegerField(choices=Sex.choices, default=Sex.NOT_SPECIFIED)
    sex_preference = models.IntegerField(
        choices=SexPreference.choices, default=SexPreference.NOT_SPECIFIED
    )

    birth_date = models.DateField(null=True)
    age_preference_min = models.IntegerField(default=-1)
    age_preference_max = models.IntegerField(default=-1)

    location = models.PointField(max_length=40, null=True)

    likes = models.ManyToManyField("self")
    dislikes = models.ManyToManyField("self")

    objects = CustomUserManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["username"]

    def __str__(self):
        return f"{self.email}"
