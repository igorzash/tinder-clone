"""User app views."""

from datetime import datetime

from django.contrib.gis.geos import Point
from django.contrib.gis.db.models.functions import Distance
from rest_framework import viewsets, permissions
from dateutil.relativedelta import relativedelta

from .models import CustomUser
from .serializers import UserSerializer


class AllowUpdateIfSameUser(permissions.BasePermission):
    """Allow users to update only their own profiles."""

    def has_permission(self, request, view):
        if request.auth is None or request.method != "PUT":
            return False

        return int(view.kwargs["pk"]) == request.user.id


class AllowAnyRegister(permissions.BasePermission):
    """Allow anyone to register."""

    def has_permission(self, request, view):
        return request.method == "POST"


class AllowListAuthenticated(permissions.BasePermission):
    """Forbid anonymous users to view user lists."""

    def has_permission(self, request, view):
        return request.auth is not None and request.method == "GET"


class UserViewSet(viewsets.ModelViewSet):  # pylint: disable=too-many-ancestors
    """User viewset."""

    queryset = CustomUser.objects.all().order_by("-date_joined")
    serializer_class = UserSerializer

    # pylint: disable=unsupported-binary-operation
    permission_classes = [
        AllowUpdateIfSameUser | AllowAnyRegister | AllowListAuthenticated
    ]

    http_method_names = ["get", "post", "put"]

    def list(self, request, *args, **kwargs):
        self.kwargs["user"] = request.user

        longitude = request.query_params.get("longitude")
        latitude = request.query_params.get("latitude")

        if longitude is not None and latitude is not None:
            self.kwargs["location"] = request.user.location = Point(
                float(longitude), float(latitude), srid=4326
            )
            request.user.save()

        return super().list(request)

    def filter_queryset(self, queryset):
        queryset = super().filter_queryset(queryset)

        user = self.kwargs.get("user")

        queryset = queryset.exclude(id=user.id)

        location = self.kwargs.get("location")

        if location is None:
            location = user.location

        if location is not None:
            queryset = queryset.annotate(
                distance=Distance("location", location)
            ).order_by("distance")

        if user.sex_preference != CustomUser.SexPreference.NOT_SPECIFIED:
            expected_sexes = [
                CustomUser.Sex.MALE,
                CustomUser.Sex.FEMALE,
                CustomUser.Sex.NOT_SPECIFIED,
            ]

            if user.sex_preference == CustomUser.SexPreference.MALE:
                expected_sexes = [CustomUser.Sex.MALE]
            elif user.sex_preference == CustomUser.SexPreference.FEMALE:
                expected_sexes = [CustomUser.Sex.FEMALE]

            queryset = queryset.filter(sex__in=expected_sexes)

        if user.sex == CustomUser.Sex.NOT_SPECIFIED:
            queryset = queryset.filter(
                sex_preference__in=[
                    CustomUser.SexPreference.NOT_SPECIFIED,
                    CustomUser.SexPreference.MALE_AND_FEMALE,
                ]
            )
        elif user.sex == CustomUser.Sex.MALE:
            queryset = queryset.filter(
                sex_preference__in=[
                    CustomUser.SexPreference.MALE,
                    CustomUser.SexPreference.MALE_AND_FEMALE,
                    CustomUser.SexPreference.NOT_SPECIFIED,
                ]
            )
        else:
            queryset = queryset.filter(
                sex_preference__in=[
                    CustomUser.SexPreference.FEMALE,
                    CustomUser.SexPreference.MALE_AND_FEMALE,
                    CustomUser.SexPreference.NOT_SPECIFIED,
                ]
            )

        age = relativedelta(datetime.now(), user.birth_date).years

        if user.age_preference_min > -1:
            queryset = queryset.exclude(
                birth_date__gt=datetime.now()
                - relativedelta(years=user.age_preference_min)
            )

        if user.age_preference_max > -1:
            queryset = queryset.exclude(
                birth_date__lt=datetime.now()
                - relativedelta(years=user.age_preference_max + 1)
            )

        if user.birth_date is not None:
            queryset = queryset.exclude(age_preference_min__gt=age).exclude(
                age_preference_max__lt=age
            )
        else:
            queryset = queryset.exclude(age_preference_min__gt=-1).exclude(
                age_preference_max__gt=-1
            )

        return queryset
