"""Model managers module."""
from django.contrib.auth.base_user import BaseUserManager


class CustomUserManager(BaseUserManager):
    """Model manager for CustomUser. Makes email unique identifier of user."""

    def create_user(self, email, password, **extra_fields):
        """Creates user."""
        if not email:
            raise ValueError("Field email must be set.")

        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)

        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, email, password, **extra_fields):
        """Creates superuser."""

        extra_fields["is_staff"] = True
        extra_fields["is_superuser"] = True
        extra_fields.setdefault("is_active", True)

        return self.create_user(email, password, **extra_fields)
