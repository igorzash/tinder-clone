"""User model serializers."""

from rest_framework import serializers, exceptions

from .models import CustomUser


class DistanceField(serializers.Field):
    """GeoDjango Distance serializer."""

    def to_representation(self, value):
        return value.km

    def to_internal_value(self, data):
        raise Exception("DistanceField to_internal_value isn't implemented.")


class UserSerializer(serializers.HyperlinkedModelSerializer):
    """User model serializer."""

    distance = DistanceField(required=False)

    class Meta:
        """UserSerializer metadata."""

        # pylint: disable=too-few-public-methods

        model = CustomUser
        fields = [
            "id",
            "username",
            "email",
            "password",
            "birth_date",
            "distance",
            "sex",
            "sex_preference",
        ]
        extra_kwargs = {
            "password": {"write_only": True, "required": False},
            "username": {"required": False},
        }

    def create(self, validated_data):
        """Creates user from validated_data."""
        if "password" not in validated_data or "username" not in validated_data:
            raise exceptions.ValidationError(detail="password or username is not set")

        password = validated_data.pop("password")

        user = CustomUser(**validated_data)
        user.set_password(password)
        user.save()

        return user
